#!/usr/bin/env python
# SPDX-License-Identifier: GPL-2.0-only
# Copyright Thomas Gleixner <tglx@linutronix.de>
#
# Mail message related code

from email.utils import make_msgid, formatdate, parseaddr
from email import message_from_string, message_from_bytes
from email.parser import BytesFeedParser
from email.generator import Generator
from email.message import Message, EmailMessage
from email.policy import EmailPolicy
from email.policy import default as DefaultPolicy
from email.headerregistry import UniqueSingleAddressHeader

import smtplib
import mailbox
import hashlib
import quopri
import base64
import time
import sys
import re

class sender_info(object):
    def __init__(self, msg):
        self.mfrom = msg.get('From')
        self.info = None

    def get_info(self):
        info = 'Sent from: %s\n' %self.mfrom
        if self.info:
            info += self.info.get_info()
        else:
            info += 'No GPG/SMIME info available'
        return info

    def store_in_msg(self, msg):
        # Convert the message into a multipart/mixed message
        ct = msg.get_content_type()
        if ct != 'multipart/mixed':
            msg.make_mixed()

        # Attach the sender information as plain/text
        msg.add_attachment(self.get_info())

        if not self.info:
            return

        # Check whether the GPG key or the S/MIME cert is
        # available and attach it.
        fname, data, maintype, subtype = self.info.get_file()
        if fname:
            if maintype != 'plain':
                msg.add_attachment(data, filename=fname, maintype=maintype,
                                   subtype=subtype)
            else:
                msg.add_attachment(data, filename=fname)

def send_smtp(msg, to, sender):
    '''
    A dumb localhost only SMTP delivery mechanism. No point in trying
    to implement the world of SMTP again. KISS rules!

    Any exception from the smtp transport is propagated to the caller
    '''
    to = msg['To']
    server = smtplib.SMTP('localhost')
    server.ehlo()
    server.send_message(msg, sender, [to])
    server.quit()

def msg_copy_headers(msgout, msg, headers):
    for key in headers:
        val = msg.get(key)
        if val:
            msgout[key] = val

def send_mail(msg, account, mfrom, sender, listheaders, use_smtp, logger,
              origfrom=None):
    '''
    Send mail to the account. Make sure that the message is correct and all
    required headers and only necessary headers are in the outgoing mail.
    '''

    # Clone the default policy (utf-8=false) and map the X-Original-From
    # header to UniqueSingleAddressHeader. Otherwise this is treated as
    # unspecified header.
    policy = DefaultPolicy.clone()
    policy.header_factory.map_to_type('X-Original-From',
                                      UniqueSingleAddressHeader)

    # Convert to EmailMessage with the modified default policy so both
    # smptlib.send_message() and the stdout dump keep the headers properly
    # encoded.
    parser = BytesFeedParser(policy=DefaultPolicy)
    parser.feed(msg.as_bytes())
    msgout = parser.close()

    # Remove all mail headers
    for k in msgout.keys():
        del msgout[k]

    # Add the required headers in the proper order
    msgout['Return-path'] = sender
    msg_copy_headers(msgout, msg, ['Date'])
    msgout['From'] = mfrom
    msgout['To'] = account.addr

    msg_copy_headers(msgout, msg, ['Subject', 'In-Reply-To', 'References',
                                   'User-Agent', 'MIME-Version', 'Charset',
                                   'Message-ID'])

    # Add the list headers
    for key, val in listheaders.items():
        msgout[key] = val

    msg_copy_headers(msgout, msg, ['Content-Type', 'Content-Disposition',
                                   'Content-Transfer-Encoding',
                                   'Content-Language'])

    if origfrom:
        msgout['X-Original-From'] = origfrom

    msgout['Envelope-To'] = get_raw_email_addr(account.addr)

    # Set unixfrom with the current date/time
    msgout.set_unixfrom('From remail ' + time.ctime(time.time()))

    if len(msgout.defects):
        logger.log_warn('Outgoing email has defects: %s\n' %msgout.defects)

    # Send it out
    if use_smtp:
        send_smtp(msgout, account.addr, sender)
    else:
        print(msgout.as_string())

# Minimal check for a valid email address
re_mail = re.compile('^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$')

def email_addr_valid(addr):
    return re_mail.match(addr)

def get_raw_email_addr(addr):
    '''
    Return the raw mail address, name and brackets stripped off.
    '''
    return parseaddr(addr)[1]

def msg_from_string(txt):
    policy = EmailPolicy(utf8=True)
    return message_from_string(txt, policy=policy)

def msg_from_bytes(txt):
    policy = EmailPolicy(utf8=True)
    return message_from_bytes(txt, policy=policy)

def msg_force_msg_id(msg, name):
    # Make sure this has a message ID
    id = msg.get('Message-ID', None)
    if not id:
        id = make_msgid(name.split('@')[0])
        msg_set_header(msg, 'Message-ID', id)

re_rmlfcr = re.compile('[\r\n]')

def msg_set_header(msg, hdr, txt):
    '''
    Set new or replace a message header
    '''
    # Sanitize the header first. Broken Outlook GPG payloads
    # come with wreckaged headers.
    txt = re_rmlfcr.sub(' ', txt)

    if msg.get(hdr):
        msg.replace_header(hdr, txt)
    else:
        msg[hdr] = txt

payload_valid_mime_headers = [
    'Content-Description',
    'Content-Transfer-Encoding',
    'Content-Disposition',
    'Content-Language',
    'Content-Type',
    'Charset',
    'Mime-Version',
]

def is_payload_header(hdr):
    for h in payload_valid_mime_headers:
        if h.lower() == hdr.lower():
            return True
    return False

def msg_set_payload(msg, payload):
    '''
    Set the payload of a message.
    '''
    msg.clear_content()

    if payload.get_content_type() == 'text/plain':
        pl = payload.get_content()
        msg.set_content(pl)
    else:
        pl = payload.get_payload()
        for h, val in payload.items():
            if is_payload_header(h):
                msg_set_header(msg, h, val)
        msg.set_payload(pl)

def msg_get_payload_as_string(msg):
    '''
    Get the payload with the associated and relevant headers
    '''
    payload = EmailMessage()
    payload.set_payload(msg.get_payload())

    for h in payload_valid_mime_headers:
        if h in msg.keys():
            payload[h] = msg[h]
        elif h == 'Content-Type':
            # Force Content-Type if not set
            # to avoid confusing gmail
            payload[h] = 'text/plain'

    return payload.as_string()

def msg_set_gpg_payload(msg, encpl, bseed, addpgp=False):
    # Create the message boundary
    boundary = hashlib.sha1('.'.join(bseed).encode()).hexdigest() + '-' * 3

    content = '--%s\n' % boundary
    content += 'Content-Type: application/pgp-encrypted\n'
    content += 'Content-Disposition: attachment\n\n'
    content += 'Version: 1\n\n'
    content += '--%s\n' % boundary
    content += 'Content-Type: application/octet-stream\n'
    content += 'Content-Disposition: attachment; filename="msg.asc"\n\n'
    if addpgp:
        content += '-----BEGIN PGP MESSAGE-----\n\n'
    content += encpl + '\n'
    if addpgp:
        content += '-----END PGP MESSAGE-----\n\n'
    content += '--%s--\n' % boundary

    msg_set_payload(msg, msg_from_string(content))
    msg_set_header(msg, 'Mime-Version', '1')
    msg_set_header(msg, 'Content-Type',
                   'multipart/encrypted; protocol="application/pgp-encrypted";boundary="%s"' % (boundary))
    msg_set_header(msg, 'Content-Disposition', 'inline')

def msg_strip_html(msg):
    '''
    Strip html from msg
    '''

    ct = msg.get_content_type()
    if ct != 'multipart/alternative':
        return

    boundary = msg.get_boundary(None)
    payload = msg.get_payload()
    stripped = False

    for m in payload:
        if m.get_content_type() == 'text/html':
            payload.remove(m)
            stripped = True

    # If no html found return
    if not stripped:
        return

    if len(payload) == 1:
        # If the remaining message is only a single item set it as payload
        msg_set_payload(msg, payload[0])
    else:
        # Recreate the multipart message
        content = 'Content-type: multipart/mixed; boundary="%s"\n\n' % boundary
        for m in payload:
            content += '--%s\n' % boundary
            content += m.as_string()
            content += '\n'
        content += '--%s--\n' % boundary
        msg_set_payload(msg, msg_from_string(content))

def msg_sanitize_outlook(msg):
    '''
    Oh well ...
    '''
    ct = msg.get_content_type()
    if ct != 'multipart/mixed':
        return

    # Try to find the payload part which actually contains the
    # magically wrapped outlook GPG data.
    # Two variants:
    # 1) random filenames provided as a plain attachement
    #    without PGP envelope
    # 2) GpgOL_MIME_structure.txt contains a fully enveloped
    #   PGP payload with the proper headers.
    # Of course everything can be base64 encoded as well...
    for payload in msg.get_payload():
        try:
            if payload.get_content_type() != 'application/octet-stream':
                continue

            fnames = ['msg.gpg', 'msg.asc', 'encrypted.asc',
                      'GpgOL_MIME_structure.txt']

            fname = payload.get_filename(None)
            if fname not in fnames:
                continue

            decode_base64(payload)
            encpl = payload.get_payload()
            # Check whether the payload is a fully enveloped PGP payload or
            # just the unwrapped msg.gpg/asc file.
            tmpmsg = message_from_string(encpl)
            if tmpmsg.get_content_type() == 'multipart/encrypted':
                msg_set_payload(msg, tmpmsg)
            else:
                msg_set_gpg_payload(msg, encpl, 'outlook', addpgp=True)
            return
        except:
            # If one of the above operations fails badly, just ignore it.
            # The unmodified message can either be handled or it will be
            # moderated/frozen. The admin has to deal with it anyway.  This
            # avoids a gazillion of conditionals and checks in the above
            # code.
            continue

def msg_handle_multimix(msg):
    '''
    Magic format used by dwmw2's devolution plugin to prevent
    exchange from wreckaging mail. It's kinda valid, but sigh...

    Multipart mixed message with:
     - empty text/plain
     - application/pgp-encrypted
     - application/octed-stream

    Make it look like a sane PGP application/encrypted mail
    '''
    ct = msg.get_content_type()
    if ct != 'multipart/mixed':
        return

    fnames = ['msg.asc', 'encrypted.asc']
    gpgpl = 0
    otherpl = []

    payloads = msg.get_payload()
    for payload in payloads:
        try:
            ct = payload.get_content_type()
            if ct == 'application/pgp-encrypted':
                gpgpl += 1
                continue
            elif ct == 'application/octet-stream':
                fname = payload.get_filename(None)
                if gpgpl == 1 and fname in fnames:
                    gpgpl += 1
                    continue
            # None of the above. Mark it as other
            otherpl.append(payload)
        except:
            # Ignore fails here. This is all best effort
            # guesswork.
            pass

    if gpgpl != 2:
        return

    # Remove the irrelevant payload parts
    for pl in otherpl:
        payloads.remove(pl)
    # Fixup the message type so decrypt knows what to do with it
    msg.set_type('multipart/encrypted')
    msg.set_param('protocol', 'application/pgp-encrypted')

def decode_base64(msg):
    #
    # Decode base64 encoded text/plain sections
    #
    if msg.get('Content-Transfer-Encoding', '') == 'base64':
        dec = base64.decodestring(msg.get_payload().encode())
        msg.set_payload(dec)
        del msg['Content-Transfer-Encoding']

def decode_alternative(msg):
    '''
    Deal with weird MUAs which put the GPG encrypted text/plain
    part into a multipart/alternative mail.
    '''
    ct = msg.get_content_type()
    if ct == 'multipart/alternative':
        payloads = msg.get_payload()
        payldec = []
        for pl in payloads:
            ct = pl.get_content_type()
            if ct == 'text/plain':
                pl = decode_base64(pl)
            payldec.append(pl)
        msg.set_payload(payldec)
    elif ct == 'text/plain':
        decode_base64(msg)

def msg_sanitize_incoming(msg):
    '''
    Get rid of HMTL, outlook, alternatives etc.
    '''
    # Strip html multipart first
    msg_strip_html(msg)

    # Handle multipart/mixed
    msg_handle_multimix(msg)

    # Sanitize outlook crappola
    msg_sanitize_outlook(msg)

    # Handle mutlipart/alternative and base64 encodings
    decode_alternative(msg)

re_noreply = re.compile('^no.?reply@')

def msg_is_autoreply(msg):
    '''
    Check whether a message is an autoreply
    '''
    # RFC 3834
    ar = msg.get('Auto-Submitted')
    if ar and ar != 'no':
        return True

    # Microsoft ...
    ar = msg.get('X-Auto-Response-Suppress')
    if ar in ('DR', 'AutoReply', 'All'):
        return True

    # precedence auto-reply
    if msg.get('Precedence', '') == 'auto-reply':
        return True

    # Reply-To is empty
    rt = msg.get('Reply-To')
    if rt:
        rt = rt.strip()
        if len(rt) == 0:
            return True

        if rt == '<>':
            return True

        # Reply-To matches no_reply, no-reply, noreply
        if re_noreply.search(rt):
            return True

    # Catch empty return path
    rp = msg.get('Return-Path')
    if not rp or rp == '<>':
        return True

    return False
